#!/usr/bin/python
import sys

def LargestPrimeFactorOf( n ):
	largestPrime = 2

	i = largestPrime
	while i <= n:
		print i, largestPrime
		if n % i == 0:
			largestPrime = i
			n /= i

		if i % 2 == 0:
			i += 1
		else:
			i += 2

	return largestPrime

print "largest prime factor = %d\n" % LargestPrimeFactorOf( int(sys.argv[1]) )
