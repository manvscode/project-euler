



def sumOfMultiplesOf3And5( n ):
	n -= 1
	sum = 0
	while n > 0:
		if n % 3 == 0 or n % 5 == 0:
			sum += n
		n -= 1
	return sum




print "10 ==> %d" % sumOfMultiplesOf3And5( 10 )
print "1000 ==> %d" % sumOfMultiplesOf3And5( 1000 )
