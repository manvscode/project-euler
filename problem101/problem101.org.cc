#include <iostream>
#include <string>
#include <vector>
using namespace std;

typedef long long BigInt;
typedef unsigned int u32;
typedef std::vector<BigInt> Series;

void printSeries( const string &name, Series s, bool bNewLine = true, ostream &out = cout );
BigInt diffSum( const Series &s, u32 start, u32 end );

inline BigInt U( BigInt n )
{
	return 1 - n + n*n - n*n*n + n*n*n*n - n*n*n*n*n + n*n*n*n*n*n - n*n*n*n*n*n*n + n*n*n*n*n*n*n*n - n*n*n*n*n*n*n*n*n + n*n*n*n*n*n*n*n*n*n;
}


Series series;

int main( int argc, char *argv[] )
{
	BigInt fitSum = 0;

	for( int i = 1; i <= 10; i++ )
	{
		series.push_back( U( i ) );
		fitSum += diffSum( series, 0, i - 1 );
	}

	cout << "FIT Sum = " << fitSum << endl;



	return 0;
}

BigInt diffSum( const Series &s, u32 start, u32 end )
{
	Series differences;

	for( u32 i = start; i < end; i++ )
	{
		differences.push_back( s[ i + 1 ] - s[ i ] );
	}

	if( start != end )
	{
		return s[ end ] + diffSum( differences, start, end - 1 );
	}
	else
	{
		return s[ end ];
	}
}



void printSeries( const string &name, Series s, bool bNewLine, ostream &out )
{
	out << "series " << name << " = { ";
	for( int i = 0; i < s.size( ); i++ )
	{
		out << s[ i ];

		if( i == s.size( ) - 1 ) 
		{
			out << " }" << endl;
		}
		else 
		{
			out << ", ";
		}
	}

	if( bNewLine ) out << endl;

}